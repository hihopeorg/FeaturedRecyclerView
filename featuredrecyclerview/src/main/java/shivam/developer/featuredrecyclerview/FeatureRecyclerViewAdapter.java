package shivam.developer.featuredrecyclerview;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.util.HashMap;
import java.util.Map;

public abstract class FeatureRecyclerViewAdapter<T extends ViewHolder> extends BaseItemProvider {

    protected Map<Integer, Integer> posHeightMap = new HashMap<>();

    public abstract T onCreateFeaturedViewHolder(ComponentContainer parent, int viewType);

    public abstract void onBindFeaturedViewHolder(T holder, int position);

    public abstract int getFeaturedItemsCount();

    public abstract void onSmallItemResize(T holder, int position, float offset);

    public abstract void onBigItemResize(T holder, int position, float offset);

    public void setPosHeightMap(Map<Integer, Integer> posHeightMap){
        this.posHeightMap = posHeightMap;
        notifyDataChanged();
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        T holder = onCreateViewHolder(componentContainer, getItemComponentType(position));
        holder.setPos(position);
        onBindViewHolder(holder, position);
        component = holder.getItemView();

        if(posHeightMap.containsKey(position)){
            ViewHolder h = (ViewHolder) component.getTag();
            h.itemView.setHeight(posHeightMap.get(position));
        }
        return component;
    }


    private T onCreateViewHolder(ComponentContainer parent, int viewType) {
        return onCreateFeaturedViewHolder(parent, viewType);
    }

    private void onBindViewHolder(T holder, int position) {
        onBindFeaturedViewHolder(holder, position);
    }

    @Override
    public int getCount() {
        return getFeaturedItemsCount();
    }

    public T getChildViewHolder(Component child){
        if(child != null){
            return (T) child.getTag();
        }else{
         return null;
        }
    }

}
