package shivam.developer.featuredrecyclerview;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.HashMap;
import java.util.Map;

public class FeaturedRecyclerView extends ListContainer {

    private int defaultItemHeight;
    private int featuredItemHeight;
    private int maxDistance;
    private int diffHeight;

    private int totalItemsInView = 0;
    private int itemToResize;
    private Map<Integer, Integer> posHeightMap = new HashMap<>();
    private Map<Integer, Integer> recPosHeightMap = new HashMap<>();
    private ListContainer.ScrolledListener scrolledListener;

    private FeatureRecyclerViewAdapter adapter;

    public FeaturedRecyclerView(Context context) {
        super(context);
        init(context, null);
    }

    public FeaturedRecyclerView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    int recScrollY = -1, recOldScrollY= -1;
    long sysTime = -1;
    private void init(Context context, AttrSet attrs) {
        initAttributes(attrs);
        scrolledListener = new ListContainer.ScrolledListener(){
            @Override
            public void onContentScrolled(Component component,  int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (getLayoutManager() instanceof DirectionalLayoutManager) {
                    DirectionalLayoutManager layoutManager = (DirectionalLayoutManager) getLayoutManager();
                    if (layoutManager.getOrientation() == Component.VERTICAL) {
                        totalItemsInView = getChildCount();
                        itemToResize = (scrollY - oldScrollY) > 0 ? 1 : 0;
                        if(sysTime == -1){
                            sysTime = System.currentTimeMillis();
                        }else{
                            if(System.currentTimeMillis() - sysTime>100){
                                sysTime = System.currentTimeMillis();
                            }else{
                                return;
                            }
                        }

                        if(recScrollY == -1){
                            recScrollY = scrollY;
                        }else{
                            if(recOldScrollY != -1 && recOldScrollY == scrollY && +recScrollY == oldScrollY){
                                return;
                            }else{
                                recScrollY = scrollY;
                            }
                        }

                        if(recOldScrollY == -1){
                            recOldScrollY = oldScrollY;
                        }else{
                            if(recScrollY != -1 && recOldScrollY == scrollY && recScrollY == oldScrollY){
                                return;
                            }else{
                                recOldScrollY = oldScrollY;
                            }
                        }

                        int height = (int)(totalItemsInView*defaultItemHeight+(featuredItemHeight-defaultItemHeight)*1.5);
                        if(scrollY > height || - oldScrollY > height){
                            return;
                        }
                        changeHeightAccordingToScroll(component);
                    }
                }
            }

            @Override
            public void scrolledStageUpdate(Component component, int newStage) {
            }
        };
        setScrolledListener(scrolledListener);
    }

    private void initAttributes(AttrSet array) {
        if(array != null && array.getAttr("defaultItemHeight") != null && array.getAttr("defaultItemHeight").isPresent()){
            defaultItemHeight = (int) array.getAttr("defaultItemHeight").get().getDimensionValue();
        }else{
            try{
                defaultItemHeight = (int) getResourceManager().getElement(ResourceTable.Float_defaultItemHeight).getFloat();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if(array != null && array.getAttr("featuredItemHeight") != null && array.getAttr("featuredItemHeight").isPresent()){
            featuredItemHeight = (int) array.getAttr("featuredItemHeight").get().getDimensionValue();
        }else{
            try{
                featuredItemHeight = (int) getResourceManager().getElement(ResourceTable.Float_featuredItemHeight).getFloat();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        diffHeight = featuredItemHeight - defaultItemHeight;
        maxDistance = featuredItemHeight;
    }

    /**
     * If the distance of view is 0 then its height would be equal to featuredItemHeight
     * else height according to distance.
     * In every case maxDistance = (featuredItemHeight + defaultItemHeight) / 2
     *
     * @param distance is distance between 0 Y-Coordinate and view.getTop()
     * @return height = = featuredItemHeight - ((distance * (featuredItemHeight - defaultItemHeight) / maxDistance)
     */
    private float height(float distance) {
        return featuredItemHeight - ((distance * (diffHeight)) / maxDistance);
    }

    /**
     * This method will invoked every time onScroll is invoked.
     *
     * @param component is the component on whom scrolling is performed.
     */
    private void changeHeightAccordingToScroll(Component component) {
        posHeightMap = new HashMap<>();

        for (int i = 0; i < totalItemsInView; i++) {

            Component viewToBeResized = getComponentAt(i);
            if (viewToBeResized != null) {
                float distance = getTopOfView(viewToBeResized);

                if (distance > maxDistance) {
                    int hh = defaultItemHeight;
                    posHeightMap.put(i, hh);
                } else if (distance <= maxDistance) {
                    int hh = (int) height(distance);
                    posHeightMap.put(i, hh);
                }

                if (i == itemToResize) {
                    onItemBigResize(viewToBeResized);
                } else {
                    onItemSmallResize(viewToBeResized);
                }
            }
        }
        getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                if(!recPosHeightMap.equals(posHeightMap)){
                    recPosHeightMap = posHeightMap;
                    adapter.setPosHeightMap(posHeightMap);
                }

                for (int i = 0; i < totalItemsInView; i++) {
                    Component viewToBeResized = getComponentAt(i);
                    if (viewToBeResized != null) {
                        onItemSmallResize(viewToBeResized);
                    }
                }
            }
        }, 100);

    }

    /**
     * Ranges from 0 to 100. If height is featuredItemHeight then offset would be
     * 100 and if height is defaultItemHeight then it will be equal to 0.
     *
     * @param height will be the height of view
     * @return the offset according to which view's child's property can be
     * changed.
     */
    private float getOffsetAccordingToHeight(int height) {
        return ((height - defaultItemHeight) * 100) / diffHeight;
    }

    private void onItemSmallResize(Component view) {
        if (adapter != null) {
            if(!(getItemProvider() instanceof FeatureRecyclerViewAdapter)) throw new RuntimeException("Adapter mast be extends FeatureRecyclerViewAdapter");

            adapter.onSmallItemResize(((FeatureRecyclerViewAdapter)getItemProvider()).getChildViewHolder(view), itemToResize, getOffsetAccordingToHeight(view.getHeight()));
        }
    }

    private void onItemBigResize(Component view) {
        if (adapter != null) {
            if(!(getItemProvider() instanceof FeatureRecyclerViewAdapter)) throw new RuntimeException("Adapter mast be extends FeatureRecyclerViewAdapter");
            adapter.onBigItemResize(((FeatureRecyclerViewAdapter)getItemProvider()).getChildViewHolder(view), itemToResize, getOffsetAccordingToHeight(view.getHeight()));
        }
    }

    private float getTopOfView(Component view) {
        return Math.abs(view.getTop());
    }

    public void setAdapter(BaseItemProvider adapter) {
        if (adapter instanceof FeatureRecyclerViewAdapter) {
            this.adapter = (FeatureRecyclerViewAdapter) adapter;
        }
        super.setItemProvider(adapter);
        scrollToF();
    }

    @Override
    public void setItemProvider(BaseItemProvider itemProvider) {
        if (adapter instanceof FeatureRecyclerViewAdapter) {
            this.adapter = (FeatureRecyclerViewAdapter) itemProvider;
        }
        super.setItemProvider(itemProvider);
        scrollToF();
    }

    //适配器设置填充后触发onContentScrolled监听
    private void scrollToF(){
        getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                scrollBy(0, 1);
            }
        }, 150);
    }


    public ScrolledListener getScrolledListener() {
        return scrolledListener;
    }
}
