/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package shivam.developer.featuredrecyclerview;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import java.util.HashMap;
import java.util.Map;

public class ViewHolder {
    private final Map<Integer, Component> views;
    public Component itemView;
    public long pos;
    public Component getItemView(){
        return itemView;
    }

    public long getPos() {
        return pos;
    }

    public void setPos(long pos) {
        this.pos = pos;
    }

    public ViewHolder(Component itemView) {
        this.views = new HashMap<>();
        if (itemView == null) {
            throw new IllegalArgumentException("itemView may not be null");
        }
        this.itemView = itemView;
        this.itemView.setTag(this);
    }

    /**
     *
     * @param viewId
     * @param value
     * @return
     */
    public ViewHolder setText(int viewId, String value) {
        Text view = getView(viewId);
        view.setText(value);
        return this;
    }

    public ViewHolder setText(int viewId, int strId) {
        Text view = getView(viewId);
        view.setText(strId);
        return this;
    }

    /**
     *
     * @param viewId
     * @param textColor
     * @return
     */
    public ViewHolder setTextColor(int viewId, int textColor) {
        Text view = getView(viewId);
        view.setTextColor(new Color(textColor));
        return this;
    }

    public void setBackgroundColor(int viewId, int color) {
        Component component = getView(viewId);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(color));
        component.setBackground(element);
    }

    public <T extends Component> T getView(int viewId) {
        Component view = views.get(viewId);
        if (view == null) {
            view = itemView.findComponentById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }

}
