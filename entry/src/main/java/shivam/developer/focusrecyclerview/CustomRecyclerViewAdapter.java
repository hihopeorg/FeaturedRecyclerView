package shivam.developer.focusrecyclerview;

import java.util.ArrayList;
import java.util.List;

//import com.squareup.picasso.Picasso;
import ohos.agp.components.*;
import shivam.developer.featuredrecyclerview.FeatureRecyclerViewAdapter;
import shivam.developer.featuredrecyclerview.ViewHolder;

public class CustomRecyclerViewAdapter extends FeatureRecyclerViewAdapter<ViewHolder> {

    private static final int ITEM_TYPE_FEATURED = 0;
    private static final int ITEM_TYPE_DUMMY = 1;

    private List<String> data = new ArrayList<>();
    private int[] images = new int[5];

    public CustomRecyclerViewAdapter() {
        images[0] = ResourceTable.Media_image_one;
        images[1] = ResourceTable.Media_image_three;
        images[2] = ResourceTable.Media_image_two;
        images[3] = ResourceTable.Media_image_four;
        images[4] = ResourceTable.Media_image_five;
    }

    @Override
    public Object getItem(int i) {
        if(data != null && data.size()>0 && i<data.size()){
            return data.get(i);
        }else if(i>=data.size()){
            return "position "+i;
        }else{
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void swapData(List<String> data) {
        this.data = data;
        if(data != null && data.size()>0){
            notifyDataSetItemRangeChanged(0, data.size());
        }else{
            notifyDataChanged();
        }
    }

    @Override
    public ViewHolder onCreateFeaturedViewHolder(ComponentContainer parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_FEATURED:
                return new FeaturedViewHolder(
                        LayoutScatter.getInstance(parent.getContext()).parse(ResourceTable.Layout_layout_featured_item, parent, false));
            case ITEM_TYPE_DUMMY:
            default:
                return new DummyViewHolder(LayoutScatter.getInstance(parent.getContext()).parse(ResourceTable.Layout_layout_dummy_item, parent, false));
        }
    }

    @Override
    public void onBindFeaturedViewHolder(ViewHolder holder, int position) {
        if (holder instanceof FeaturedViewHolder) {
            FeaturedViewHolder featuredViewHolder = (FeaturedViewHolder) holder;
            //todo edit
//            Picasso.get().load(images[position % 4]).tag(holder.itemView.getContext()).into(featuredViewHolder.ivBackground);
            featuredViewHolder.ivBackground.setPixelMap(images[position % 4]);
            featuredViewHolder.tvHeading.setText(data.get(position));
        } else if (holder instanceof DummyViewHolder) {
            //Do nothing
        }
    }

    @Override
    public int getFeaturedItemsCount() {
        return data.size() + 2; /* Return 2 extra dummy items */
    }

    @Override
    public int getItemComponentType(int position) {
        return position >= 0 && position < data.size() ? ITEM_TYPE_FEATURED : ITEM_TYPE_DUMMY;
    }

    @Override
    public void onSmallItemResize(ViewHolder holder, int position, float offset) {
        if (holder instanceof FeaturedViewHolder) {
            FeaturedViewHolder featuredViewHolder = (FeaturedViewHolder) holder;
            featuredViewHolder.tvHeading.setAlpha(offset / 100f);
        }
    }

    @Override
    public void onBigItemResize(ViewHolder holder, int position, float offset) {
        if (holder instanceof FeaturedViewHolder) {
            FeaturedViewHolder featuredViewHolder = (FeaturedViewHolder) holder;
            featuredViewHolder.tvHeading.setAlpha(offset / 100f);
        }
    }


    static class FeaturedViewHolder extends ViewHolder {

        Image ivBackground;
        Text tvHeading;

        FeaturedViewHolder(Component itemView) {
            super(itemView);
            ivBackground = (Image) itemView.findComponentById(ResourceTable.Id_iv_background);
            tvHeading = (Text) itemView.findComponentById(ResourceTable.Id_tv_heading);
        }
    }

    static class DummyViewHolder extends ViewHolder {

        DummyViewHolder(Component itemView) {
            super(itemView);
        }
    }
}
