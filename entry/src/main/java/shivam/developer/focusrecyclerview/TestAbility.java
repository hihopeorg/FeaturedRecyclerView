package shivam.developer.focusrecyclerview;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TestAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_test);
    }
}
