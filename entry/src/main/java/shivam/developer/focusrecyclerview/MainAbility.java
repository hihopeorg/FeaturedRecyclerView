package shivam.developer.focusrecyclerview;

import ohos.agp.components.Component;
import shivam.developer.featuredrecyclerview.FeatureLinearLayoutManager;
import shivam.developer.featuredrecyclerview.FeaturedRecyclerView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;
import java.util.List;

public class MainAbility extends Ability {

    List<String> dummyData = new ArrayList<>();
    FeaturedRecyclerView featuredRecyclerView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_main);
        createDummyDataList();

        featuredRecyclerView = (FeaturedRecyclerView) findComponentById(ResourceTable.Id_featured_recycler_view);
        FeatureLinearLayoutManager layoutManager = new FeatureLinearLayoutManager(this);
        layoutManager.setOrientation(Component.VERTICAL);
        featuredRecyclerView.setLayoutManager(layoutManager);

        CustomRecyclerViewAdapter adapter = new CustomRecyclerViewAdapter();
        adapter.swapData(dummyData);

        featuredRecyclerView.setAdapter(adapter);

    }

    private void createDummyDataList() {
        for (int i = 1; i <= 20; i++) {
            dummyData.add("Item " + i);
        }
    }
}