package featuredrecyclerview;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.*;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import shivam.developer.featuredrecyclerview.FeatureRecyclerViewAdapter;
import shivam.developer.featuredrecyclerview.FeaturedRecyclerView;
import shivam.developer.featuredrecyclerview.ViewHolder;
import shivam.developer.focusrecyclerview.ResourceTable;
import shivam.developer.focusrecyclerview.TestAbility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class FeatureRecyclerViewAdapterTest {

    IAbilityDelegator sAbilityDelegator;
    TestAbility ability;
    Context context;
    FeatureRecyclerViewAdapter adapter;
    private List<String> data = new ArrayList<>();
    protected Map<Integer, Integer> posHeightMap = new HashMap<>();
    FeaturedRecyclerView listContainer;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(TestAbility.class);
        EventHelper.sleep(2000);
        sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        posHeightMap.put(0, 330);

        for (int i = 1; i <= 20; i++) {
            data.add("Item " + i);
        }
        adapter = new FeatureRecyclerViewAdapter() {
            @Override
            public ViewHolder onCreateFeaturedViewHolder(ComponentContainer parent, int viewType) {
                return new ViewHolder(LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_featured_item, null, false));
            }

            @Override
            public void onBindFeaturedViewHolder(ViewHolder holder, int position) {
                Text tvHeading = (Text) holder.getView(ResourceTable.Id_tv_heading);
                holder.setText(ResourceTable.Id_tv_heading, "test "+position);
            }

            @Override
            public int getFeaturedItemsCount() {
                return data.size();
            }

            @Override
            public void onSmallItemResize(ViewHolder holder, int position, float offset) {
                holder.getView(ResourceTable.Id_tv_heading).setAlpha(offset / 100f);
            }

            @Override
            public void onBigItemResize(ViewHolder holder, int position, float offset) {
                holder.getView(ResourceTable.Id_tv_heading).setAlpha(offset / 100f);
            }

            @Override
            public Object getItem(int i) {
                return data.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }
        };
        listContainer = new FeaturedRecyclerView(context);
        listContainer.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        listContainer.setOrientation(Component.VERTICAL);
        DirectionalLayout root = (DirectionalLayout) ability.findComponentById(ResourceTable.Id_root);
        sAbilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                root.addComponent(listContainer, ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
                listContainer.setAdapter(adapter);
            }
        });
    }

    @After
    public void tearDown() throws Exception {
        sAbilityDelegator.stopAbility(ability);
        EventHelper.clearAbilities();
        EventHelper.sleep(2000);
    }

    @Test
    public void onCreateFeaturedViewHolder() {
        EventHelper.sleep(1000);
        sAbilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                ViewHolder holder = adapter.onCreateFeaturedViewHolder(null, 0);
                assertNotNull(holder);
            }
        });
    }

    @Test
    public void getFeaturedItemsCount() {
        EventHelper.sleep(1000);
        assertTrue(adapter.getFeaturedItemsCount() == data.size());
    }

    @Test
    public void onSmallItemResize() {
        EventHelper.sleep(1000);
        Component c = listContainer.getComponentAt(0);
        ViewHolder holder = adapter.getChildViewHolder(c);
        sAbilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                adapter.onSmallItemResize(holder, 0, 1);
            }
        });
        Text tvHeading = (Text) holder.getView(ResourceTable.Id_tv_heading);
        assertTrue(tvHeading.getAlpha() == 1/100f);
    }

    @Test
    public void onBigItemResize() {
        EventHelper.sleep(1000);
        Component c = listContainer.getComponentAt(0);
        ViewHolder holder = adapter.getChildViewHolder(c);
        sAbilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                adapter.onSmallItemResize(holder, 0, 99);
            }
        });
        Text tvHeading = (Text) holder.getView(ResourceTable.Id_tv_heading);
        assertTrue(tvHeading.getAlpha() == 99/100f);
    }

    @Test
    public void setPosHeightMap() {
        EventHelper.sleep(1000);
        sAbilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                adapter.setPosHeightMap(posHeightMap);
            }
        });
        EventHelper.sleep(1000);
        assertTrue(listContainer.getComponentAt(0).getHeight() == posHeightMap.get(0));
    }

    @Test
    public void getChildViewHolder() {
        EventHelper.sleep(1000);
        Component c = listContainer.getComponentAt(0);
        ViewHolder holder = adapter.getChildViewHolder(c);
        assertNotNull(holder);
        assertTrue(holder.getPos() == 0);
    }
}