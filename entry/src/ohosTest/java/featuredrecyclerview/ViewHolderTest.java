package featuredrecyclerview;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import shivam.developer.featuredrecyclerview.ViewHolder;
import shivam.developer.focusrecyclerview.ResourceTable;
import shivam.developer.focusrecyclerview.TestAbility;

import static org.junit.Assert.*;

public class ViewHolderTest {

    IAbilityDelegator sAbilityDelegator;
    TestAbility ability;
    Context context;
    ViewHolder viewHolder;
    Component c;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(TestAbility.class);
        EventHelper.sleep(2000);
        sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        c = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_featured_item, null, false);
        viewHolder = new ViewHolder(c);
    }

    @After
    public void tearDown() throws Exception {
        sAbilityDelegator.stopAbility(ability);
        EventHelper.clearAbilities();
        EventHelper.sleep(2000);
    }

    @Test
    public void getItemView() {
        assertEquals(viewHolder.getItemView(), c);
    }

    @Test
    public void getPos() {
        viewHolder.setPos(0);
        assertTrue(viewHolder.getPos() == 0);
    }

    @Test
    public void setText() {
        Text tvHeading = (Text) viewHolder.getView(ResourceTable.Id_tv_heading);
        viewHolder.setText(ResourceTable.Id_tv_heading, "test");
        assertEquals(tvHeading.getText(), "test");
    }


    @Test
    public void testSetText() {
        Text tv  = (Text)viewHolder.getView(ResourceTable.Id_tv_heading);
        viewHolder.setText(ResourceTable.Id_tv_heading, context.getString(ResourceTable.String_app_name));
        assertEquals(tv.getText(), context.getString(ResourceTable.String_app_name));
    }

    @Test
    public void setTextColor() {
        viewHolder.setTextColor(ResourceTable.Id_tv_heading, ResourceTable.Color_colorPrimary);
        Text tvHeading = (Text) viewHolder.getView(ResourceTable.Id_tv_heading);
        assertEquals(tvHeading.getTextColor().getValue(), ResourceTable.Color_colorPrimary);
    }

    @Test
    public void setBackgroundColor() {
        viewHolder.setBackgroundColor(ResourceTable.Id_tv_heading, context.getColor(ResourceTable.Color_colorPrimary));
        Text tvHeading = (Text) viewHolder.getView(ResourceTable.Id_tv_heading);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(context.getColor(ResourceTable.Color_colorPrimary)));
        ShapeElement tmpElement = (ShapeElement) tvHeading.getBackgroundElement();
        assertEquals(tmpElement.getRgbColors(), element.getRgbColors());
    }

    @Test
    public void getView() {
        Image ivBackground = viewHolder.getView(ResourceTable.Id_iv_background);
        Image tmp = (Image) viewHolder.getItemView().findComponentById(ResourceTable.Id_iv_background);
        assertSame(ivBackground, tmp);
    }
}