package featuredrecyclerview;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import shivam.developer.featuredrecyclerview.FeaturedRecyclerView;
import shivam.developer.focusrecyclerview.CustomRecyclerViewAdapter;
import shivam.developer.focusrecyclerview.ResourceTable;
import shivam.developer.focusrecyclerview.TestAbility;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FeaturedRecyclerViewTest {

    IAbilityDelegator sAbilityDelegator;
    TestAbility ability;
    Context context;
    CustomRecyclerViewAdapter adapter;
    FeaturedRecyclerView listContainer;
    boolean flag;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(TestAbility.class);
        EventHelper.sleep(2000);
        sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
        List<String> dummyData = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            dummyData.add("Item " + i);
        }
        adapter = new CustomRecyclerViewAdapter();
        adapter.swapData(dummyData);
        listContainer = new FeaturedRecyclerView(context);
        listContainer.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        listContainer.setOrientation(Component.VERTICAL);
        DirectionalLayout root = (DirectionalLayout) ability.findComponentById(ResourceTable.Id_root);
        sAbilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                root.addComponent(listContainer, ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
            }
        });

    }

    @After
    public void tearDown() throws Exception {
        sAbilityDelegator.stopAbility(ability);
        EventHelper.clearAbilities();
        EventHelper.sleep(2000);
    }

    @Test
    public void setAdapter() {
        flag = false;
        ListContainer.ScrolledListener scrolledListener = listContainer.getScrolledListener();
        if(scrolledListener != null){
            listContainer.removeScrolledListener(scrolledListener);
            scrolledListener = null;
        }

        listContainer.setScrolledListener(new ListContainer.ScrolledListener(){
            @Override
            public void onContentScrolled(Component component,  int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                flag = true;
            }

            @Override
            public void scrolledStageUpdate(Component component, int newStage) {
            }
        });
        sAbilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                listContainer.setAdapter(adapter);
            }
        });

        EventHelper.sleep(2000);
        assertTrue(flag);
    }

    @Test
    public void setItemProvider() {
        flag = false;
        ListContainer.ScrolledListener scrolledListener = listContainer.getScrolledListener();
        if(scrolledListener != null){
            listContainer.removeScrolledListener(scrolledListener);
            scrolledListener = null;
        }

        listContainer.setScrolledListener(new ListContainer.ScrolledListener(){
            @Override
            public void onContentScrolled(Component component,  int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                flag = true;
            }

            @Override
            public void scrolledStageUpdate(Component component, int newStage) {
            }
        });
            sAbilityDelegator.runOnUIThreadSync(new Runnable() {
                @Override
                public void run() {
                    listContainer.setItemProvider(adapter);
                }
            });

        EventHelper.sleep(2000);
        assertTrue(flag);
    }
}