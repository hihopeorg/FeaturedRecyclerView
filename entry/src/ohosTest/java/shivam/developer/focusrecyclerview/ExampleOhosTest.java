package shivam.developer.focusrecyclerview;

import featuredrecyclerview.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import shivam.developer.featuredrecyclerview.FeaturedRecyclerView;

import java.sql.ClientInfoStatus;

import static org.junit.Assert.*;

public class ExampleOhosTest {

    IAbilityDelegator sAbilityDelegator;
    MainAbility ability;
    Context context;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        EventHelper.sleep(2000);
        sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
    }

    @After
    public void tearDown() throws Exception {
        sAbilityDelegator.stopAbility(ability);
        EventHelper.clearAbilities();
        EventHelper.sleep(2000);
    }

    @Test
    public void testActive() {
        EventHelper.sleep(1500);
        FeaturedRecyclerView featuredRecyclerView = (FeaturedRecyclerView) ability.findComponentById(ResourceTable.Id_featured_recycler_view);
        Component c = featuredRecyclerView.getComponentAt(1);
        Text t = (Text) c.findComponentById(ResourceTable.Id_tv_heading);
        int height = c.getHeight();
        float alpha = t.getAlpha();
        EventHelper.inputSwipe(ability, featuredRecyclerView, 848, 1248, 848, 345, 200);
        EventHelper.sleep(1000);
        c = featuredRecyclerView.getComponentAt(1);
        t = (Text) c.findComponentById(ResourceTable.Id_tv_heading);
        assertTrue(c.getHeight() > height);
        assertTrue(t.getAlpha() > alpha);
    }
}