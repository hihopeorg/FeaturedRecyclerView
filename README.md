# FeaturedRecyclerView

**本项目是基于开源项目FeaturedRecyclerView进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/developer-shivam/FeaturedRecyclerView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：开源FeaturedRecyclerView项目
- 所属系列：ohos的第三方组件适配移植
- 功能：自定义视图组扩展ListContainer使得位于顶部的第一个项为最大项（通过将其高度设置为featuredItemHeight）。
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/developer-shivam/FeaturedRecyclerView
- 原项目基线版本：1.0.0，shal:622c6731c40393abb7f5ec1a235bfbf76dfa8ce5
- 编程语言：Java 
- 外部库依赖：无

#### 效果展示

![效果展示](art/sample.gif)

#### 安装教程

方法1.
1. 编译依赖库har包FeaturedRecyclerView.har。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'shivam.developer.focusrecyclerview:featuredrecyclerview:1.0.0'
}
```




#### 使用说明

##### 属性
* **featuredItemHeight**: 第一项的高度。
* **defaultItemHeight** : 其它项的高度。
* **offset** : 它不是一个xml属性，而是在**FeatureRecyclerViewAdapter**的方法中接收的一个参数，用于设置子部件属性的动画。

##### 图解视图
![图解视图](art/diagram_small.jpg) 

##### 概念
![概念](art/concept.gif) 

##### 使用
* 定义使用
```java
        featuredRecyclerView = (FeaturedRecyclerView) findComponentById(ResourceTable.Id_featured_recycler_view);
        FeatureLinearLayoutManager layoutManager = new FeatureLinearLayoutManager(this);
        layoutManager.setOrientation(Component.VERTICAL);
        featuredRecyclerView.setLayoutManager(layoutManager);

        CustomRecyclerViewAdapter adapter = new CustomRecyclerViewAdapter();
        adapter.swapData(dummyData);

        featuredRecyclerView.setAdapter(adapter);
```
* 布局使用
```xml
<shivam.developer.featuredrecyclerview.FeaturedRecyclerView
        ohos:id="$+id:featured_recycler_view"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:orientation="vertical"
        app:defaultItemHeight="110vp"
        app:featuredItemHeight="300vp"/>
```

使用**FeaturedRecycleServiceAdapter**的优点在于，它还包含两种方法，可用于设置childs属性属性的动画（如textView淡入淡出）。

```java
    @Override
    public void onSmallItemResize(CustomRecyclerViewHolder holder, int position, float offset) {
       holder.tvHeading.setAlpha(offset / 100f);
    }

    @Override
    public void onBigItemResize(CustomRecyclerViewHolder holder, int position, float offset) {
       holder.tvHeading.setAlpha(offset / 100f);
    }
```





#### 版本迭代

- v1.0.0

- 位于ListContainer顶部的第一个项一直是最大状态并且内部文字在滑动中有淡入淡出效果

##### 效果差异说明

- 原项目是通过监听滑动事件后直接修改item高度进行视图修改，而鸿蒙项目中只能通过BaseItemProvider指定position的高度然后调用notifyDataChanged()方法后更新布局，
并且原项目在设定数据源后可以多次触发监听事件更新视图，本项目中在设置数据源后设置滚动1vp来触发一次监听事件，
由此存在差异表现为：填充数据源后初始加载原项目文字的的透明度都为半透。在滑动如1dp后第二项大小快速切换为普通正常大小并且文字透明度由上到下逐渐递减。
而在本项目中表现为设置数据源后初始加载文字透明度由上到下逐渐递减，而且在滑到第二项（及为最大项）后后面的项变大小一致。而且在滑动中该项目中表现item大小跳动，不够平滑。

    


#### 版权和许可信息
```
Copyright 2017 Shivam Satija

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
